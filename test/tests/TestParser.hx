package tests;

import htmlparser.HtmlNodeElement;
import utest.Assert;
import torznab.types.Parser;
import utest.ITest;


@:access(torznab.types.Parser)
class TestParser implements ITest
{
    public function new()
    {

    }


    function testParseDate()
    {
        var d1: Date = Parser.parseDate('Mon, 12 Dec 2022 16:33:10 +0200');
        Assert.equals(12, d1.getDate());
        Assert.equals(11, d1.getMonth());
        Assert.equals(2022, d1.getFullYear());
        Assert.equals(18, d1.getHours());
        Assert.equals(33, d1.getMinutes());
        Assert.equals(10, d1.getSeconds());

        // test all months
        for (i in 0...12)
        {
            var months: Array<String> = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec' ];
            var d2: Date = Parser.parseDate('Mon, 12 ${months[i]} 2022 16:33:10 +0000');

            Assert.equals(i, d2.getMonth());
        }

        var d3: Date = Parser.parseDate('Mon, 12 Dec 2022 16:33:10 -0200');
        Assert.equals(12, d3.getDate());
        Assert.equals(11, d3.getMonth());
        Assert.equals(2022, d3.getFullYear());
        Assert.equals(14, d3.getHours());
        Assert.equals(33, d3.getMinutes());
        Assert.equals(10, d3.getSeconds());

        // test invalid date
        Assert.raises(() -> Parser.parseDate('invalid date'));

        // test invalid month
        Assert.raises(() -> Parser.parseDate('Mon, 12 Asd 2022 16:33:10 +0000'));
    }


    function testMissingXmlElements()
    {
        var node: HtmlNodeElement = new HtmlNodeElement('div', []);

        Assert.equals(null, Parser.stringAttr(node, 'elem', 'attr'));
        Assert.equals(null, Parser.intAttr(node, 'elem', 'attr'));
        Assert.equals(null, Parser.string(node, 'elem'));
        Assert.equals(null, Parser.int(node, 'elem'));
        Assert.equals(null, Parser.bool(node, 'elem'));
    }


    function testParseError()
    {
        var node: HtmlNodeElement = new HtmlNodeElement('div', []);

        // test invalid error
        Assert.equals(null, Parser.parseError(node.toString()));
    }
}
