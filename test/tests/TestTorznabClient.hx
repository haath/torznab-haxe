package tests;

import torznab.TorznabClient;
import torznab.types.Capabilities;
import torznab.types.SearchResult;
import utest.Assert;
import utest.Async;
import utest.ITest;

using TestUtil;


@await
class TestTorznabClient implements ITest
{
    inline var baseUrl: String = 'http://torznab:9117';

    var apiKey: String;
    var testObject: TorznabClient;

    public function new()
    {
    }

    @:timeout(10000) @await
    function setupClass(async: Async)
    {
        apiKey = @await TestUtil.getApiKey(baseUrl);

        @await TestUtil.installIndexers(baseUrl);

        async.done();
    }

    function setup()
    {
        testObject = new TorznabClient('$baseUrl/api/v2.0/indexers/all/results/torznab', apiKey);
    }

    function teardown()
    {
    }

    @:timeout(10000) @await
    function testCapabilities(async: Async)
    {
        var caps: Capabilities = @await testObject.getCapabilities();

        Assert.equals('Jackett', caps.server.title);
        Assert.equals(1000, caps.limits.max);
        Assert.equals(1000, caps.limits._default);

        Assert.equals(true, caps.searching.search.available);
        Assert.equals('q', caps.searching.search.supportedParams.join(','));
        Assert.equals('raw', caps.searching.search.searchEngine);
        Assert.equals(true, caps.searching.tvSearch.available);
        Assert.isTrue(caps.searching.tvSearch.supportedParams.indexOf('season') > -1);
        Assert.equals('raw', caps.searching.tvSearch.searchEngine);
        Assert.equals(true, caps.searching.movieSearch.available);
        Assert.isTrue(caps.searching.movieSearch.supportedParams.indexOf('q') > -1);
        Assert.equals('raw', caps.searching.movieSearch.searchEngine);
        Assert.equals(true, caps.searching.musicSearch.available);
        Assert.equals('q,album,artist', caps.searching.musicSearch.supportedParams.join(','));
        Assert.equals('raw', caps.searching.musicSearch.searchEngine);
        Assert.equals(true, caps.searching.audioSearch.available);
        Assert.equals('q,album,artist', caps.searching.audioSearch.supportedParams.join(','));
        Assert.equals('raw', caps.searching.audioSearch.searchEngine);
        Assert.equals(true, caps.searching.bookSearch.available);
        Assert.equals('q', caps.searching.bookSearch.supportedParams.join(','));
        Assert.equals('raw', caps.searching.bookSearch.searchEngine);

        Assert.equals(8, caps.categories.length);
        Assert.equals(1000, caps.categories[ 0 ].id);
        Assert.equals('Console', caps.categories[ 0 ].name);
        Assert.equals(9, caps.categories[ 0 ].subCategories.length);
        Assert.equals(1010, caps.categories[ 0 ].subCategories[ 0 ].id);
        Assert.equals('Console/NDS', caps.categories[ 0 ].subCategories[ 0 ].name);

        async.done();
    }

    @:timeout(10000) @await
    function testSearch(async: Async)
    {
        var res: SearchResult = @await testObject.search('avengers');

        Assert.equals('AggregateSearch', res.title);
        Assert.equals('This feed includes all configured trackers', res.description);
        Assert.equals('http://127.0.0.1/', res.link);
        Assert.equals('en-US', res.language);
        Assert.equals('search', res.category);

        Assert.isTrue(res.items.length > 0);
        for (item in res.items)
        {
            Assert.notNull(item.title);
            Assert.notNull(item.guid);
            Assert.notNull(item.type);
            Assert.notNull(item.comments);
            Assert.notNull(item.pubDate);
            Assert.isTrue(item.size > 0);
            Assert.notNull(item.descrption);
            Assert.notNull(item.link);
            Assert.notNull(item.seeders);
            Assert.notNull(item.peers);
            Assert.notNull(item.downloadVolumeFactor);

            Assert.isTrue(item.indexer == 'rarbg' || item.indexer == '1337x' || item.indexer == 'thepiratebay', 'got indexer ${item.indexer}');
        }

        async.done();
    }

    @:timeout(10000) @await
    function testSearchTv(async: Async)
    {
        var res: SearchResult = @await testObject.search('punisher',
            {
                searchType: Tv,
                attributes: [ 'tvdbid' ],
                limit: 10
            });

        Assert.equals(10, res.items.length);
        for (item in res.items)
        {
            var found: Bool = false;

            for (cat in item.categories)
            {
                found = found || (cat >= 5000 || cat < 6000);
            }

            Assert.isTrue(found, 'expected categories 5000 but got ${item.categories}');
        }

        async.done();
    }

    @:timeout(10000) @await
    function testSearchMovie(async: Async)
    {
        var res: SearchResult = @await testObject.search('pulp fiction',
            {
                searchType: Movie,
                categories: [ 2050, 2040 ],
                extended: true,
                offset: 1
            });

        Assert.isTrue(res.items.length > 0);
        for (item in res.items)
        {
            var found: Bool = false;

            for (cat in item.categories)
            {
                found = found || (cat == 2050 || cat == 2040);
            }

            Assert.isTrue(found, 'expected categories 2050,2040 but got ${item.categories}');
        }

        async.done();
    }

    @:timeout(10000)
    function testWithoutApiKey(async: Async)
    {
        @:privateAccess testObject.apiKey = null;

        var b1: Async = async.branch();
        var b2: Async = async.branch();

        testObject.getCapabilities().handle(o -> switch o
        {
            case Success(_):
                Assert.fail();
                b1.done();

            case Failure(_):
                Assert.pass();
                b1.done();
        });

        testObject.search('avengers').handle(o -> switch o
        {
            case Success(_):
                Assert.fail();
                b2.done();

            case Failure(_):
                Assert.pass();
                b2.done();
        });
    }
}
