import tink.CoreApi;
import haxe.Json;

#if nodejs
    typedef Http = http.HttpNodeJs;
#elseif js
    typedef Http = http.HttpJs;
#else
    typedef Http = haxe.Http;
#end


@await
class TestUtil
{
    static var loginCookie: String;


    @async
    public static function getApiKey(url: String): String
    {
        if (Sys.getEnv('TORZNAB_API_KEY') != null)
        {
            return Sys.getEnv('TORZNAB_API_KEY');
        }

        if (loginCookie == null)
        {
            var loginUrl: String = '$url/UI/Login';
            loginCookie = @await httpGetCookie(loginUrl);
        }

        var configUrl: String = '$url/api/v2.0/server/config';
        var configJson: String = @await httpGet(configUrl, loginCookie);

        var obj: Dynamic = Json.parse(configJson);

        return obj.api_key;
    }


    @async
    public static function installIndexers(url: String)
    {
        if (loginCookie == null)
        {
            var loginUrl: String = '$url/UI/Login';
            loginCookie = @await httpGetCookie(loginUrl);
        }

        for (indexer in [ 'rarbg', '1337x', 'thepiratebay' ])
        {
            var installUrl: String = '$url/api/v2.0/indexers/$indexer/config';
            @await httpPost(installUrl, loginCookie, @await httpGet(installUrl, loginCookie));
        }

        return null;
    }


    public static function httpGet(url: String, cookie: String): Promise<String>
    {
        var trigger: PromiseTrigger<String> = Promise.trigger();

        var http: Http = new Http(url);

        http.addHeader('Cookie', cookie);

        http.onData = (data: String) ->
        {
            trigger.resolve(data);
        };
        http.onStatus = (status: Int) ->
        {

        };
        http.onError = (err: String) ->
        {
            trigger.reject(new Error(InternalError, err));
        };
        http.request();

        return trigger.asPromise();
    }


    public static function httpGetCookie(url: String): Promise<String>
    {
        var trigger: PromiseTrigger<String> = Promise.trigger();

        var http: Http = new Http(url);

        http.onData = (data: String) ->
        {
            var cookie: String = http.responseHeaders['Set-Cookie'];
            if (cookie == null)
            {
                cookie = http.responseHeaders['set-cookie'];
            }
            trigger.resolve(cookie);
        };
        http.onStatus = (status: Int) ->
        {

        };
        http.onError = (err: String) ->
        {
            trigger.reject(new Error(InternalError, err));
        };
        http.request();

        return trigger.asPromise();
    }


    public static function httpPost(url: String, cookie: String, data: String): Promise<Noise>
    {
        var trigger: PromiseTrigger<Noise> = Promise.trigger();
        var http: Http = new Http(url);

        http.addHeader('Cookie', cookie);
        http.setPostData(data);
        http.setHeader('Content-Type', 'application/json');
        http.setHeader('X-Requested-With', 'XMLHttpRequest');

        http.onData = (data: String) ->
        {
            trigger.resolve(null);
        };
        http.onStatus = (status: Int) ->
        {

        };
        http.onError = (err: String) ->
        {
            trigger.reject(new Error(InternalError, err));
        };
        http.request(true);

        return trigger.asPromise();
    }
}
