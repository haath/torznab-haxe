package torznab.types.capabilities;


typedef SubCategory =
{
    var id: Int;
    var name: String;
}
