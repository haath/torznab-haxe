package torznab.types.capabilities;


typedef SearchCapability =
{
    var available: Bool;
    var supportedParams: Array<String>;
    var searchEngine: String;
}
