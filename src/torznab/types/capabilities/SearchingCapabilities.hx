package torznab.types.capabilities;

import torznab.types.capabilities.*;


typedef SearchingCapabilities =
{
    var search: SearchCapability;
    var tvSearch: SearchCapability;
    var movieSearch: SearchCapability;
    var musicSearch: SearchCapability;
    var audioSearch: SearchCapability;
    var bookSearch: SearchCapability;
}
