package torznab.types.capabilities;


typedef Limits =
{
    var max: Null<Int>;
    var _default: Null<Int>;
}
