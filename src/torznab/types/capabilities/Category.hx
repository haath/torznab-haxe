package torznab.types.capabilities;

import torznab.types.capabilities.SubCategory;


typedef Category = SubCategory &
{
    var subCategories: Array<SubCategory>;
}
