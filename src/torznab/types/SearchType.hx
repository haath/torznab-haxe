package torznab.types;


enum SearchType
{
    Generic;
    Tv;
    Movie;
    Music;
    Audio;
    Book;
}
