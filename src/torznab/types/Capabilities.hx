package torznab.types;

import torznab.types.capabilities.*;


typedef Capabilities =
{
    var server: Server;

    var searching: SearchingCapabilities;

    var limits: Limits;

    var categories: Array<Category>;
}
