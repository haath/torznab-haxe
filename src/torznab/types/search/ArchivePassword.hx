package torznab.types.search;


enum abstract ArchivePassword(Int)
{
    var None = 0;
    var RarPassword = 1;
    var InnerArchive = 2;
}
