package torznab.types.search;

import torznab.types.search.SeedType;
import torznab.types.search.IndexerType;


/**
 * Type representing a single torrent from a torznab search.
 *
 * Since the specification is fluid, at the time of development the following
 * two sources were considered when constructing this type:
 * - The spec at: https://torznab.github.io/spec-1.3-draft/torznab/Specification-v1.3.html#predefined-attributes
 * - Resonses from Jackett: https://github.com/Jackett/Jackett
 */
typedef SearchResultItem =
{
    /** The title of the release. */
    var title: String;
    /** The type of the tracker the release was found on. */
    var type: IndexerType;
    /** The date the release was published. */
    var pubDate: Date;
    /** The description of the release on the tracker. */
    var descrption: String;
    /** The link to the release; usually the same as `magnetUrl`. */
    var link: String;
    /** Size in bytes */
    var size: Null<Float>;
    /** Category ids. */
    var categories: Array<Int>;
    /** Individual tags. */
    var tags: Array<String>;
    /** Unique release guid. */
    var guid: String;
    /** Number of files in the release */
    var files: Null<Int>;
    /** Username that uploaded the release to the tracker. */
    var poster: String;
    /** Team doing the release. (Release Group name) */
    var team: String;
    /** Number of times the release was fetched from the indexer. */
    var grabs: Null<Int>;
    /** Number of active seeders according to the tracker(s). */
    var seeders: Null<Int>;
    /** Number of active leechers (non-seeders) according to the tracker(s). */
    var leechers: Null<Int>;
    /** Number of active peers (seeders and leechers) according to the tracker(s). */
    var peers: Null<Int>;
    /** Torrent infohash as lower-case hexadecimal: String. */
    var infohash: String;
    /** Magnet uri. */
    var magnetUrl: String;
    /** Specifies which seed criteria must be met. Possible values are: ratio, seedtime, both or either (default). */
    var seedType: SeedType;
    /** Minimum ratio required before the torrent client may stop seeding. */
    var minimumRatio: Null<Float>;
    /** Minimum time in seconds that the torrent must be actively seeded by the client before it may be stopped. */
    var minimumSeedTime: Null<Float>;
    /** Multiplier for the download volume that counts toward the user’s account on the tracker. Freeleech would be 0.0. */
    var downloadVolumeFactor: Null<Float>;
    /** Multiplier for the upload volume that counts toward the user’s account on the tracker. */
    var uploadVolumeFactor: Null<Float>;
    /** Whether the archive is passworded (`0`=no, `1`=rar password, `2`=contains inner archive) */
    var password: Null<ArchivePassword>;
    /** Number of comments or url to comments page */
    var comments: String;
    /** Whether an .nfo file is available. (0 no, 1 yes) */
    var nfo: Null<Bool>;
    /** Url for the .nfo file. */
    var info: String;
    /** Release year */
    var year: Null<Int>;
    /** Cover image url */
    var coverUrl: String;
    /** Backdrop image url */
    var backdropUrl: String;
    /** Critics review */
    var review: String;
    /** Season number */
    var season: Null<Int>;
    /** Episode number */
    var episode: Null<Int>;
    /** Detected TVRage ID. (TVRage.com, now defunct) */
    var rageId: Null<Int>;
    /** Human readable Show Title (originally from TV Rage) */
    var tvTitle: String;
    /** Air date for the episode (eg. Tue, 22 Jun 2010 06:54:22 +0100) */
    var tvAirdate: Date;
    /** TheTVDB show id associated with this release. */
    var tvdbId: Null<Int>;
    /** TVMaze show id associated with this release. */
    var tvmazeId: Null<Int>;
    /**: Genre (eg. Horror, Comedy) */
    var genre: String;
    /**: Video Codec (eg. x264) */
    var video: String;
    /**  audio 	string 	Audio Codec (eg. AC3 2.0 @ 384 kbs) */
    var audio: String;
    /**: Video resolution (eg. 1280x716 1.78:1) */
    var resolution: String;
    /**: Video framerate (eg. 23.976 fps) */
    var framerate: Null<Float>;
    /**  audio 	string 	Video/Audio languages (eg. English) */
    var language: String;
    /**: Subtitle languages (eg. English, Spanish) */
    var subs: String;
    /** Imdb ID. (www.imdb.com) */
    var imdb: String;
    /** Imdb score (eg. 5/10) */
    var imdbScore: String;
    /** Imdb title */
    var imdbTitle: String;
    /** Imdb tagline */
    var imdbTagline: String;
    /** Imdb plot */
    var imdbPlot: String;
    /** Imdb year */
    var imdbYear: Null<Int>;
    /** Imdb director */
    var imdbDirector: String;
    /** Imdb actors */
    var imdbActors: String;
    /** Artist name */
    var artist: String;
    /** Album name */
    var album: String;
    /** Publisher name */
    var publisher: String;
    /** Track listing (eg. Track 1|Track 2|Track 3) */
    var tracks: String;
    /** Book title */
    var bookTitle: String;
    /** Date the book was published */
    var publishDate: Date;
    /** Booth author */
    var author: String;
    /** Number of book pages */
    var pages: Null<Int>;
    /** The indexer the release was found on (Jackett) */
    var indexer: String;
}