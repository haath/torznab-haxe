package torznab.types.search;


enum abstract IndexerType(String)
{
    var Private = 'private';
    var Public = 'public';
    var SemiPrivate = 'semi-private';
}
