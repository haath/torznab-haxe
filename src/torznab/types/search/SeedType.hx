package torznab.types.search;


enum abstract SeedType(String)
{
    var Ratio = 'ratio';
    var SeedTime = 'seedtime';
    var Both = 'both';
    var Either = 'either';
}
