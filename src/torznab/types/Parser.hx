package torznab.types;

import torznab.types.search.SearchResultItem;
import torznab.types.capabilities.SubCategory;
import torznab.types.capabilities.Category;
import torznab.types.capabilities.SearchCapability;
import htmlparser.HtmlNodeElement;
import htmlparser.XmlDocument;

using DateTools;
using StringTools;
using torznab.types.Parser;


class Parser
{
    public static function parseCapabilities(xml: String): Capabilities
    {
        var doc: HtmlNodeElement = new XmlDocument(xml).children[0];
        if (doc.name != 'caps')
        {
            return null;
        }

        function parseSearchCapability(tag: String): SearchCapability
        {
            var xmlTag: HtmlNodeElement = doc.find('>searching>$tag')[0];

            return {
                available: xmlTag.getAttribute('available') == 'yes',
                supportedParams: xmlTag.getAttribute('supportedParams').split(','),
                searchEngine: xmlTag.getAttribute('searchEngine')
            };
        }

        function parseSubCategory(node: HtmlNodeElement): SubCategory
        {
            return {
                id: Std.parseInt(node.getAttribute('id')),
                name: node.getAttribute('name'),
            }
        }

        function parseCategory(node: HtmlNodeElement): Category
        {
            return {
                id: Std.parseInt(node.getAttribute('id')),
                name: node.getAttribute('name'),
                subCategories: node.find('>subcat').map(parseSubCategory)
            };
        }

        return
        {
            server: {
                title: doc.stringAttr('>server', 'title')
            },
            searching: {
                search: parseSearchCapability('search'),
                tvSearch: parseSearchCapability('tv-search'),
                movieSearch: parseSearchCapability('movie-search'),
                musicSearch: parseSearchCapability('music-search'),
                audioSearch: parseSearchCapability('audio-search'),
                bookSearch: parseSearchCapability('book-search'),
            },
            limits: {
                max: doc.intAttr('>limits', 'max'),
                _default: doc.intAttr('>limits', 'default'),
            },
            categories: doc.find('>categories>category').map(parseCategory)
        };
    }


    public static function parseSearchResult(xml: String): SearchResult
    {
        var doc: HtmlNodeElement = new XmlDocument(xml).children[0];
        if (doc.name != 'rss')
        {
            return null;
        }
        var channel: HtmlNodeElement = doc.children[0];

        function parseSearchItem(node: HtmlNodeElement): SearchResultItem
        {
            return {
                title: node.string('>title'),
                type: cast node.string('>type'),
                pubDate: parseDate(node.string('>pubDate')),
                descrption: node.string('>description'),
                link: node.string('>link'),
                size: node.float('>size'),
                categories: node.intArray('>category'),
                tags: node.stringArray('>tags'),
                guid: node.string('>guid'),
                files: node.int('>files'),
                poster: node.string('>poster'),
                team: node.string('>team'),
                grabs: node.int('>grabs'),
                seeders: node.int('>seeders'),
                leechers: node.int('>leechers'),
                peers: node.int('>peers'),
                infohash: node.string('>infohash'),
                magnetUrl: node.string('>magneturl'),
                seedType: cast node.string('>seedtype'),
                minimumRatio: node.float('>minimumratio'),
                minimumSeedTime: node.float('>minimumseedtime'),
                downloadVolumeFactor: node.float('>downloadvolumefactor'),
                uploadVolumeFactor: node.float('>uploadvolumefactor'),
                password: cast node.int('>password'),
                comments: node.string('>comments'),
                nfo: node.bool('>nfo'),
                info: node.string('>info'),
                year: node.int('>year'),
                coverUrl: node.string('>coverurl'),
                backdropUrl: node.string('>backdropurl'),
                review: node.string('>review'),
                season: node.int('>season'),
                episode: node.int('>episode'),
                rageId: node.int('>rageid'),
                tvTitle: node.string('>tvtitle'),
                tvAirdate: parseDate(node.string('>tvairdate')),
                tvdbId: node.int('>tvdbid'),
                tvmazeId: node.int('>tvmazeid'),
                genre: node.string('>genre'),
                video: node.string('>video'),
                audio: node.string('>audio'),
                resolution: node.string('>resolution'),
                framerate: node.float('>resolution'),
                language: node.string('>language'),
                subs: node.string('>subs'),
                imdb: node.string('>imdb'),
                imdbScore: node.string('>imdbscore'),
                imdbTitle: node.string('>imdbtitle'),
                imdbTagline: node.string('>imdbtagline'),
                imdbPlot: node.string('>imdbplot'),
                imdbYear: node.int('>imdbyear'),
                imdbDirector: node.string('>imdbdirector'),
                imdbActors: node.string('>imdbactors'),
                artist: node.string('>artist'),
                album: node.string('>album'),
                publisher: node.string('>publisher'),
                tracks: node.string('>tracks'),
                bookTitle: node.string('>booktitle'),
                publishDate: parseDate(node.string('>publishdate')),
                author: node.string('>author'),
                pages: node.int('>pages'),
                indexer: node.stringAttr('>jackettindexer', 'id'),
            };
        }

        return {
            title: channel.string('>title'),
            description: channel.string('>description'),
            link: channel.string('>link'),
            language: channel.string('>language'),
            category: channel.string('>category'),
            items: channel.find('>item').map(parseSearchItem),
        };
    }


    static function stringArray(doc: HtmlNodeElement, elem: String): Array<String>
    {
        var values: Array<String> = [];

        if (doc.find(elem).length > 0)
        {
            values = doc.find(elem).map(node -> node.innerText);
        }
        else
        {
            var attrName: String = elem.replace('>', '');
            values = doc.find('>torznab:attr')
                        .filter(node -> node.getAttribute('name') == attrName)
                        .map(node -> node.getAttribute('.value'));
        }

        return values;
    }


    static function intArray(doc: HtmlNodeElement, elem: String): Array<Int>
    {
        return doc.stringArray(elem).map(value -> cast(Std.parseInt(value), Int));
    }


    public static function parseError(xml: String): String
    {
        var doc: HtmlNodeElement = new XmlDocument(xml).children[0];
        if (doc.name != 'error')
        {
            return null;
        }

        return doc.getAttribute('description');
    }


    static function string(doc: HtmlNodeElement, elem: String): String
    {
        var value: String = null;

        if (doc.find(elem).length > 0)
        {
            value = doc.find(elem)[0].innerText;
        }
        else
        {
            var attrName: String = elem.replace('>', '');
            for (node in doc.find('>torznab:attr'))
            {
                if (node.getAttribute('name') == attrName)
                {
                    value = node.getAttribute('value');
                    break;
                }
            }
        }

        return value;
    }


    static function int(doc: HtmlNodeElement, elem: String): Null<Int>
    {
        var str: String = doc.string(elem);
        if (str == null)
        {
            return null;
        }

        return Std.parseInt(str);
    }


    static function bool(doc: HtmlNodeElement, elem: String): Null<Bool>
    {
        var str: String = doc.string(elem);
        if (str == null)
        {
            return null;
        }

        return str == '1';
    }


    static function float(doc: HtmlNodeElement, elem: String): Null<Float>
    {
        var str: String = doc.string(elem);
        if (str == null)
        {
            return null;
        }

        return Std.parseFloat(str);
    }


    static function stringAttr(doc: HtmlNodeElement, elem: String, attr: String): String
    {
        if (doc.find(elem).length == 0)
        {
            return null;
        }

        return doc.find(elem)[0].getAttribute(attr);
    }


    static function intAttr(doc: HtmlNodeElement, elem: String, attr: String): Null<Int>
    {
        var str: String = doc.stringAttr(elem, attr);
        if (str == null)
        {
            return null;
        }

        return Std.parseInt(str);
    }


    static function parseDate(dateStr: String): Date
    {
        if (dateStr == null)
        {
            return null;
        }

        var expr: EReg = ~/(\d\d) (\w\w\w) (\d\d\d\d) (\d\d):(\d\d):(\d\d) (\+|-)(\d\d)(\d\d)/;
        if (!expr.match(dateStr))
        {
            throw 'unable to parse date string: $dateStr';
        }

        var utcOffsetHours: Int = Std.parseInt(expr.matched(8));
        var utcOffsetMinutes: Int = Std.parseInt(expr.matched(9));
        if (expr.matched(7) == '-')
        {
            utcOffsetHours = -utcOffsetHours;
            utcOffsetMinutes = -utcOffsetMinutes;
        }

        var month: Int = switch expr.matched(2)
        {
            case 'Jan': 0; case 'Feb': 1; case 'Mar': 2; case 'Apr': 3;
            case 'May': 4; case 'Jun': 5; case 'Jul': 6; case 'Aug': 7;
            case 'Sep': 8; case 'Oct': 9; case 'Nov': 10; case 'Dec': 11;

            default: throw 'invalid month name';
        };

        var date: Date = new Date(
            Std.parseInt(expr.matched(3)),
            month,
            Std.parseInt(expr.matched(1)),
            Std.parseInt(expr.matched(4)),
            Std.parseInt(expr.matched(5)),
            Std.parseInt(expr.matched(6))
        );

        return date.delta(utcOffsetHours.hours() + utcOffsetMinutes.minutes());
    }
}
