package torznab.types;

import torznab.types.SearchType;


typedef SearchOptions =
{
    /** The type of search to perform (generic, movie, etc..) */
    var ?searchType: SearchType;
    /** List of category ids that should be included in the search; should be obtained from the server's capabilities */
    var ?categories: Array<Int>;
    /** List of extended attributes that should be included in the search results; see: https://torznab.github.io/spec-1.3-draft/torznab/Specification-v1.3.html#extended-attributes */
    var ?attributes: Array<String>;
    /** Specifies that all extended attributes should be included in the results; overrules `attributes` */
    var ?extended: Bool;
    /** Number of items to skip in the result */
    var ?offset: Int;
    /** Number of results to return; limited by the `Limits` element in the `Capabilities` */
    var ?limit: Int;
}
