package torznab.types;

import torznab.types.search.SearchResultItem;


typedef SearchResult =
{
    var title: String;
    var description: String;
    var link: String;
    var language: String;
    var category: String;

    var items: Array<SearchResultItem>;
}
