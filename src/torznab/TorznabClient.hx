package torznab;

import haxe.Http;
import tink.CoreApi;
import torznab.api.ApiMethod;
import torznab.types.Capabilities;
import torznab.types.Parser;
import torznab.types.SearchOptions;
import torznab.types.SearchResult;


class TorznabClient
{
    var baseUrl: String;
    var apiKey: String;

    public function new(url: String, ?apiKey: String)
    {
        baseUrl = url;
        this.apiKey = apiKey;
    }

    /**
     * Get the capabilities of the Torznab feed.
     *
     * https://torznab.github.io/spec-1.3-draft/torznab/Specification-v1.3.html#capabilities
     *
     * @return the capabilities object
     */
    public function getCapabilities(): Promise<Capabilities>
    {
        return
            httpGet(Capabilities).map(o -> switch o
            {
                case Success(xml):
                    {
                        var caps: Capabilities = Parser.parseCapabilities(xml);
                        if (caps != null)
                        {
                            Success(caps);
                        }
                        else
                        {
                            Failure(new Error(InternalError, Parser.parseError(xml)));
                        }
                    }

                case Failure(failure):
                    Failure(failure);
            });
    }

    /**
     * Search for torrents.
     *
     * @param query the query to search for
     * @param options options used to limit the search
     * @return the search results
     */
    public function search(query: String, ?options: SearchOptions): Promise<SearchResult>
    {
        var method: ApiMethod = Search;
        var parameters: Map<String, String> = [ 'q' => query ];

        if (options != null)
        {
            method = switch options.searchType
                {
                    case Generic:
                        Search;
                    case Tv:
                        SearchTv;
                    case Movie:
                        SearchMovie;
                    case Music:
                        SearchMusic;
                    case Audio:
                        SearchAudio;
                    case Book:
                        SearchBook;
                    default:
                        Search;
                };

            if (options.categories != null)
            {
                parameters.set('cat', options.categories.join(','));
            }
            if (options.attributes != null)
            {
                parameters.set('attrs', options.attributes.join(','));
            }
            if (options.extended == true)
            {
                parameters.set('extended', '1');
            }
            if (options.offset != null)
            {
                parameters.set('offset', Std.string(options.offset));
            }
            if (options.limit != null)
            {
                parameters.set('limit', Std.string(options.limit));
            }
        }

        return
            httpGet(Search, parameters).map(o -> switch o
            {
                case Success(xml):
                    {
                        var searchResult: SearchResult = Parser.parseSearchResult(xml);
                        if (searchResult != null)
                        {
                            Success(searchResult);
                        }
                        else
                        {
                            Failure(new Error(InternalError, Parser.parseError(xml)));
                        }
                    }

                case Failure(failure):
                    Failure(failure);
            });
    }

    function httpGet(method: ApiMethod, ?parameters: Map<String, String>): Promise<String>
    {
        var req: Http = new Http(baseUrl);

        req.addParameter('t', cast method);

        if (apiKey != null)
        {
            req.addParameter('apikey', apiKey);
        }

        if (parameters != null)
        {
            for (name => value in parameters)
            {
                req.addParameter(name, value);
            }
        }

        var resp: PromiseTrigger<String> = Promise.trigger();

        req.onStatus = (status: Int) ->
        {
            switch status
            {
                case 200:
                // success

                default:
                    resp.trigger(Failure(new Error(status, 'http error')));
            };
        };
        req.onData = (data: String) ->
        {
            resp.trigger(Success(data));
        };
        req.request();

        return resp.asFuture();
    }
}
