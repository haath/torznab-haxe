package torznab.api;


enum abstract ApiMethod(String)
{
    /**
     * Returns the capabilities of the api.
     */
    var Capabilities = 'caps';
    /**
     * Free text search query.
     */
    var Search = 'search';
    /**
     * Search query with tv specific query params and filtering.
     */
    var SearchTv = 'tvsearch';
    /**
     * Search query with movie specific query params and filtering.
     */
    var SearchMovie = 'movie';
    /**
     * Search query with music specific query params and filtering.
     */
    var SearchMusic = 'music';
    /**
     * Search query with music specific query params and filtering.
     */
    var SearchAudio = 'audio';
    /**
     * Search query with book specific query params and filtering.
     */
    var SearchBook = 'book';
}
