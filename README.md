<div align="center">

### torznab-haxe

[![build status](https://gitlab.com/haath/torznab-haxe/badges/master/pipeline.svg)](https://gitlab.com/haath/torznab-haxe/pipelines)
[![test coverage](https://gitlab.com/haath/torznab-haxe/badges/master/coverage.svg)](https://gitlab.com/haath/torznab-haxe/-/jobs/artifacts/master/browse?job=test-neko)
[![license](https://img.shields.io/badge/license-MIT-blue.svg?style=flat)](https://gitlab.com/haath/torznab/blob/master/LICENSEs)
[![haxelib version](https://badgen.net/haxelib/v/torznab)](https://lib.haxe.org/p/torznab)
[![haxelib downloads](https://badgen.net/haxelib/d/torznab)](https://lib.haxe.org/p/torznab)

</div>

---

Simple library for interfacing with a [torznab](https://torznab.github.io/spec-1.3-draft/torznab/Specification-v1.3.html) API.


## Installation

Package is available on Haxelib.

```sh
haxelib install torznab
```


## Usage

Get the API url and key of the Torznab endpoint. In the following example [Jackett](https://github.com/Jackett/Jackett) is used.

```haxe
var client: TorznabClient = new TorznabClient('http://jackett:9117/api/v2.0/indexers/all/results/torznab', 'api key');
```

Note that all methods return [tink_core](https://github.com/haxetink/tink_core) promises.
As such they are compatible with the rest of the tink framework.
For the examples here, it is assumed that [tink_await](https://github.com/haxetink/tink_await) is also installed, but it is not a dependency of this library.

If needed, fetch the server's capabilities.

```haxe
var caps: Capabilities = @await client.getCapabilities();

trace(caps.limits.max); // server supports fetching a maximum of this many results on each search

if (caps.searching.tvSearch.available)
{
    // server supports search for tv shows
}
```

Then search for torrents using the `search()` method.

```haxe
var result: SearchResult = @await client.search('the avengers');

for (torrent in result.items)
{
    trace(torrent.magnetUrl);
}
```

Searches can be narrowed down with the second optional argument.

```haxe
client.search('the avengers', {
    searchType: Movie,
    categories: [ 2045, 2050 ], // UHD and BluRay; obtained from the server's capabilities
    limit: 10
});
```
